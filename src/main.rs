use clap::{Parser, Subcommand};
use pulsechain_cli::{blocks, status, validators, DEFAULT_RPC_ENDPOINT};

#[derive(Parser)]
#[clap(author, version, about)]
struct Cli {
    /// Sets the URL to use for RPC calls
    #[clap(
        short,
        long,
        default_value = DEFAULT_RPC_ENDPOINT,
        value_name = "URL"
    )]
    endpoint: String,

    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Print the general network status
    Status,
    /// Commands relating to blocks
    #[clap(subcommand)]
    Blocks(Blocks),
    /// Commands relating to validators
    #[clap(subcommand)]
    Vals(Vals),
}

#[derive(Subcommand)]
enum Blocks {
    /// Print details about recent blocks
    List {
        /// The number of recent blocks to fetch
        count: u8,
        /// The block fields to print (defaults: number, author, hash). Can use "all"
        /// See https://docs.rs/web3/latest/web3/types/struct.Block.html#fields
        #[clap(short, long)]
        field: Vec<String>,
    },
    /// Print details about a specific block
    Show {
        /// The block number to fetch
        number: u64,
        /// The block fields to print (defaults: number, author, hash). Can use "all"
        /// See https://docs.rs/web3/latest/web3/types/struct.Block.html#fields
        #[clap(short, long)]
        field: Vec<String>,
    },
}

#[derive(Subcommand)]
enum Vals {
    /// Print the validators currently in rotation
    List,
    /// Print all registered validators
    Reg,
}

#[tokio::main]
async fn main() -> web3::Result<()> {
    let cli = Cli::parse();

    let transport = web3::transports::Http::new(&cli.endpoint)?;
    let web3 = web3::Web3::new(transport);
    println!("Connecting to blockchain at {}...\n", &cli.endpoint);

    match &cli.command {
        Commands::Status => {
            let chain_status = status::fetch(web3).await;
            print!("{}", chain_status);
        }
        Commands::Blocks(query) => match query {
            Blocks::List { count, field } => {
                let blocks = blocks::fetch_recent(web3, *count, field.to_owned()).await;
                print!("{}", blocks);
            }
            Blocks::Show { number, field } => {
                let block = blocks::fetch_one(web3, *number, field.to_owned()).await;
                print!("{}", block);
            }
        },
        Commands::Vals(query) => match query {
            Vals::List => {
                let validators = validators::list(web3).await;
                print!("{}", validators);
            }
            Vals::Reg => {
                let registered = validators::registered(web3).await;
                print!("{}", registered);
            }
        },
    }

    Ok(())
}
