use futures::future;
use web3::{
    types::{H160, U256},
    Transport, Web3,
};

use crate::{
    blocks::{self, Blocks},
    utils::as_pls_str,
    validators::{self, Validators},
    BURN_CONTRACT_ADDR, CURRENCY_SYMBOL, SLASHING_CONTRACT_ADDR, STAKING_CONTRACT_ADDR,
    VALIDATOR_CONTRACT_ADDR,
};

/// Basic stats about the overall network status.
pub struct PulseChainStatus {
    balances: ContractBalances,
    latest_blocks: Blocks,
    validators: Validators,
}

struct ContractBalances {
    burn_contract: U256,
    slash_contract: U256,
    staking_contract: U256,
    validator_contract: U256,
}

/// Fetches basic stats about the overall network status.
pub async fn fetch<T: Transport>(web3: Web3<T>) -> PulseChainStatus {
    let eth = web3.eth();

    let burn_contract_addr = BURN_CONTRACT_ADDR.parse::<H160>().unwrap();
    let slashing_contract_addr = SLASHING_CONTRACT_ADDR.parse::<H160>().unwrap();
    let staking_contract_addr = STAKING_CONTRACT_ADDR.parse::<H160>().unwrap();
    let validator_contract_addr = VALIDATOR_CONTRACT_ADDR.parse::<H160>().unwrap();

    let (burn_balance, slash_balance, staking_balance, validator_balance) = future::join4(
        eth.balance(burn_contract_addr, None),
        eth.balance(slashing_contract_addr, None),
        eth.balance(staking_contract_addr, None),
        eth.balance(validator_contract_addr, None),
    )
    .await;

    let (latest_blocks, validators) = future::join(
        blocks::fetch_recent(
            web3.clone(),
            1,
            vec![
                "number".to_string(),
                "hash".to_string(),
                "timestamp".to_string(),
                "author".to_string(),
            ],
        ),
        validators::list(web3),
    )
    .await;

    let balances = ContractBalances {
        burn_contract: burn_balance.unwrap(),
        slash_contract: slash_balance.unwrap(),
        staking_contract: staking_balance.unwrap(),
        validator_contract: validator_balance.unwrap(),
    };

    PulseChainStatus {
        balances,
        latest_blocks,
        validators,
    }
}

#[allow(unused_must_use)]
impl std::fmt::Display for PulseChainStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let block_number = self.latest_blocks[0].number.unwrap().as_u64();
        let blocks_until_rotation = 28800 - (block_number % 28800);

        // print contract balances
        writeln!(f, "CONTRACT BALANCES");
        writeln!(
            f,
            "  Burn Contract: {} {}",
            as_pls_str(self.balances.burn_contract),
            CURRENCY_SYMBOL
        );
        writeln!(
            f,
            "  Slashing Contract: {} {}",
            as_pls_str(self.balances.slash_contract),
            CURRENCY_SYMBOL
        );
        writeln!(
            f,
            "  Staking Contract: {} {}",
            as_pls_str(self.balances.staking_contract),
            CURRENCY_SYMBOL
        );
        writeln!(
            f,
            "  Validator Contract: {} {}",
            as_pls_str(self.balances.validator_contract),
            CURRENCY_SYMBOL
        );

        // print the latest block details
        writeln!(f);
        write!(f, "LATEST {}", self.latest_blocks);

        // print the current validator set
        writeln!(f, "{}", self.validators);

        let est_time = chrono::Local::now()
            + chrono::Duration::seconds(((blocks_until_rotation * 3) as i64).into());

        // print next rotation info
        writeln!(
            f,
            "NEXT ROTATION IN {} BLOCKS (est. {})",
            blocks_until_rotation,
            est_time.format("%r")
        );

        writeln!(f)
    }
}
