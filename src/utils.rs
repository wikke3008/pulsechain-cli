use web3::types::U256;

/// Humanizes the input wei into a Pulse string.
/// ```
/// use pulse_validator_stats::as_pls_str;
/// use web3::types::U256;
///
/// let wei = U256::from_dec_str("2196306700000000000").unwrap();
/// assert_eq!(as_pls_str(wei), "2.1963067".to_string());
///
/// let wei = U256::from_dec_str("7000000000000000000").unwrap();
/// assert_eq!(as_pls_str(wei), "7".to_string());
///
/// let wei = U256::from_dec_str("12300000").unwrap();
/// assert_eq!(as_pls_str(wei), "0.0000000000123".to_string());
/// ```
pub fn as_pls_str(wei: U256) -> String {
    let wei = format!("{:0>19}", format!("{}", wei));
    let partition = wei.len() - 18;

    let head = wei[..partition].to_string();
    let tail = wei[partition..].to_string();

    format!("{}.{}", head, tail)
        .trim_end_matches('0')
        .trim_end_matches('.')
        .to_string()
}
