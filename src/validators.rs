use futures::future;
use web3::{
    contract::{tokens::Tokenizable, Contract, Options},
    types::{H160, U256},
    Transport, Web3,
};

use crate::{
    utils::as_pls_str, CURRENCY_SYMBOL, MAX_CONCURRENCY, SLASHING_CONTRACT_ADDR,
    STAKING_CONTRACT_ADDR, VALIDATOR_CONTRACT_ADDR,
};

/// A collection of registered validator details.
pub struct RegisteredValidators(Vec<RegisteredValidator>);

/// Details about a single registered validator.
pub struct RegisteredValidator {
    _active: bool,
    addr: H160,
    fee_addr: H160,
    percent_rev_share: u32,
    in_rotation: bool,
    pending_stake: U256,
    total_stake: U256,
}

/// A collection of active validator details.
pub struct Validators {
    last_block: u64,
    validators: Vec<Validator>,
}

/// Details about a single active validator.
pub struct Validator {
    address: H160,
    slash_count: U256,
    last_slash_block: u64,
}

impl Tokenizable for RegisteredValidators {
    fn from_token(token: web3::ethabi::Token) -> Result<Self, web3::contract::Error>
    where
        Self: Sized,
    {
        let mut registered = vec![];
        if let web3::ethabi::Token::Array(tokens) = token {
            for token in tokens {
                if let web3::ethabi::Token::Tuple(tokens) = token {
                    registered.push(RegisteredValidator {
                        _active: tokens[0].to_owned().into_bool().unwrap(),
                        addr: tokens[1].to_owned().into_address().unwrap(),
                        fee_addr: tokens[2].to_owned().into_address().unwrap(),
                        percent_rev_share: tokens[3].to_owned().into_uint().unwrap().as_u32(),
                        in_rotation: tokens[4].to_owned().into_bool().unwrap(),
                        pending_stake: tokens[5].to_owned().into_uint().unwrap(),
                        total_stake: tokens[6].to_owned().into_uint().unwrap(),
                    });
                }
            }
        }

        Ok(RegisteredValidators(registered))
    }

    fn into_token(self) -> web3::ethabi::Token {
        todo!("Not needed!")
    }
}

/// Fetches the list of current validators in rotation.
pub async fn list<T: Transport>(web3: Web3<T>) -> Validators {
    let eth = web3.eth();
    let validator_contract_addr = VALIDATOR_CONTRACT_ADDR.parse::<H160>().unwrap();
    let validator_contract = Contract::from_json(
        eth.clone(),
        validator_contract_addr,
        include_bytes!("../res/validatorset.abi"),
    )
    .unwrap();
    let slashing_contract_addr = SLASHING_CONTRACT_ADDR.parse::<H160>().unwrap();
    let slashing_contract = Contract::from_json(
        eth.clone(),
        slashing_contract_addr,
        include_bytes!("../res/slashing.abi"),
    )
    .unwrap();

    let current_validators: Vec<H160> = validator_contract
        .query("getValidators", (), None, Options::default(), None)
        .await
        .unwrap();
    let mut validators = vec![];

    let mut tasks = vec![];
    for (i, v) in current_validators.iter().enumerate() {
        tasks.push(async {
            let slashes =
                slashing_contract.query("getSlashIndicator", *v, None, Options::default(), None);
            let (last_slash_block, slash_count): (U256, U256) = slashes.await.unwrap();

            Validator {
                address: *v,
                slash_count,
                last_slash_block: last_slash_block.as_u64(),
            }
        });

        if tasks.len() == MAX_CONCURRENCY || i == current_validators.len() - 1 {
            validators.append(&mut future::join_all(tasks).await);
            tasks = vec![];
        }
    }

    let last_block = web3.eth().block_number().await.unwrap().as_u64();

    Validators {
        last_block,
        validators,
    }
}

/// Fetches the full list of registered validators.
pub async fn registered<T: Transport>(web3: Web3<T>) -> RegisteredValidators {
    let eth = web3.eth();
    let staking_contract_addr = STAKING_CONTRACT_ADDR.parse::<H160>().unwrap();

    let staking_contract = Contract::from_json(
        eth.clone(),
        staking_contract_addr,
        include_bytes!("../res/staking.abi"),
    )
    .unwrap();

    let registered: RegisteredValidators = staking_contract
        .query(
            "listRegisteredValidators",
            (),
            None,
            Options::default(),
            None,
        )
        .await
        .unwrap();

    registered
}

#[allow(unused_must_use)]
impl std::fmt::Display for RegisteredValidators {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "REGISTERED VALIDATORS");
        for (idx, val) in self.0.iter().enumerate() {
            writeln!(f, "  {:0>2}: Address: 0x{:x}", idx + 1, val.addr);
            writeln!(f, "      Fee Address: 0x{:x}", val.fee_addr);
            writeln!(f, "      In Rotation: {}", val.in_rotation);
            writeln!(f, "      Rev Share: {}%", val.percent_rev_share);
            writeln!(
                f,
                "      Pending Stake: {} {}",
                as_pls_str(val.pending_stake),
                CURRENCY_SYMBOL
            );
            writeln!(
                f,
                "      Total Stake: {} {}",
                as_pls_str(val.total_stake),
                CURRENCY_SYMBOL
            );
            writeln!(f);
        }
        Ok(())
    }
}
#[allow(unused_must_use)]
impl std::fmt::Display for Validators {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "CURRENT VALIDATORS");
        for (idx, val) in self.validators.iter().enumerate() {
            write!(
                f,
                "  {:0>2}: 0x{:x} | {} slashes",
                idx + 1,
                val.address,
                val.slash_count,
            );
            if val.slash_count.as_usize() > 0 {
                write!(
                    f,
                    " ({} blocks ago)",
                    self.last_block - val.last_slash_block,
                );
            }
            writeln!(f);
        }
        Ok(())
    }
}
